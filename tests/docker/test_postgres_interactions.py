import pytest

from main import all_upload_actions


@pytest.mark.parametrize("action", all_upload_actions())
def test_postgres_actions(action):
    action.reset()
    action.run()
