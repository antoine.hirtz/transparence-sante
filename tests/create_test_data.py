import os

from src.constants.csv import COMPANY_GROUP_CSV, COMPANY_CSV
from src.constants.directories import TEST, RAW_DATA_DIR

TEST_ROW_LIMIT = 100

RAW_TEST_DATA_DIR = os.path.join(TEST, RAW_DATA_DIR)


def head(input_file_path, number_of_lines, output_file_path):
    with open(input_file_path, 'rb') as input_file, open(output_file_path, 'wb') as output_file:
        for i, line in enumerate(input_file):
            if i >= number_of_lines:
                break
            output_file.write(line)


def create_test_data():
    for file_name in os.listdir(RAW_DATA_DIR):
        if not file_name.endswith('.csv'):
            continue

        raw_file_path = os.path.join(RAW_DATA_DIR, file_name)
        raw_test_file_path = os.path.join(RAW_TEST_DATA_DIR, file_name)
        if file_name == COMPANY_GROUP_CSV or file_name == COMPANY_CSV:
            number_of_lines = 10**5
        else:
            number_of_lines = TEST_ROW_LIMIT

        print("Take {} first lines from '{}' to write '{}'".format(number_of_lines, raw_file_path, raw_test_file_path))
        head(raw_file_path, number_of_lines, raw_test_file_path)


if __name__ == '__main__':
    create_test_data()
