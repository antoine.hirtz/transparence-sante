#!/usr/bin/env python3
import sys

from src.constants.miscellaneous import ERROR_CODE_INVALID_ARGUMENT

remove_images_message = """
This command remove all Docker containers and images from this project.
Container data that is mapped on host will be kept.
Re-building docker images make take a long time.
"""

serve_dev_website_message = """
This command builds the website and serve it at url http://localhost:4000.
Website is rebuilt dynamically after every modification.
Internal hyperlinks are correct, whereas the production version use eurosfordocs.fr as the base url.
Stop and restart if you modify '_config.yml' file.
"""

build_python_message = "Rebuild Python image with current code."

run_h2_message = "Stop metabase and run H2 container to connect to metabase data."

process_prod_message = "Process data and stop Metabase during upload"

ACTION_MAPPING = {
    'up': ("", "docker-compose up --detach"),
    'down': ("", "docker-compose down"),
    'build-python': (build_python_message,
                     "docker-compose build python"),
    'pytest': ("", "docker-compose run --rm python pytest"),
    'python': ("", "docker-compose run --rm python"),
    'build-prod-website': ("", "docker-compose up website"),
    'serve-dev-website': (serve_dev_website_message,
                          "docker-compose -f docker-compose.yml -f docker-compose.dev.yml up website"),
    'rmi': (remove_images_message,
            "docker-compose down --rmi local --volumes"),
    'h2': (run_h2_message,
           "docker-compose stop metabase; "
           "docker-compose -f docker-compose.yml -f docker-compose.dev.yml up h2"),
    'process-prod': (process_prod_message,
                     "docker-compose run --rm python ./main.py clean; "
                     "docker-compose stop metabase; "
                     "docker-compose run --rm python ./main.py reset-upload; "
                     "docker-compose run --rm python ./main.py upload; "
                     "docker-compose up -d nginx"
                     )
}


def invalid_input():
    print("Helper script to use Docker-Compose\n")
    print("{:<20}  {}".format("Action", "Command"))
    for action_key in ACTION_MAPPING.keys():
        print_action(action_key)
    exit(ERROR_CODE_INVALID_ARGUMENT)


def print_action(action_key):
    message, command = ACTION_MAPPING[action_key]
    print("  {:<20}{}".format(action_key, command))
    print_message(message)


def print_message(message):
    for line in message.split('\n'):
        if line:
            print("{:26}{}".format('', line))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        invalid_input()

    action_key = sys.argv[1]
    if action_key in ACTION_MAPPING:
        message, command = ACTION_MAPPING[action_key]
        print("Command :")
        print(command)
        print(message)
    else:
        invalid_input()
