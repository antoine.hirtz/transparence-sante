import json
import os
import warnings

import pandas as pd
import pandas_profiling

from settings import CSV_CHUNK_SIZE
from settings import ROWS_LIMIT
from src.constants.csv import DEFAULT_SEPARATOR
from src.constants.directories import ROOTED_RAW_DATA_DIR, ROOTED_CLEANED_DATA_DIR
from src.generic import validate
from src.utils.dataframe_io import read_csv_by_chunk, write_df_chunk_iterator, get_csv_columns
from src.utils.utils import prefix, timeit

RAW_PROFILE_REPORT_DIR = os.path.join('website', 'technical', 'raw_data_profile')
CLEANED_PROFILE_REPORT_DIR = os.path.join('website', 'technical', 'cleaned_data_profile')


class Cleaner:
    """ Wrapper around pandas to clean a raw csv and write it in a new directory

    Usage :
    - Start by creating an empty cleaner with argument `csv_name` and optional `input_separator` and `input_csv_dir`
    - Execute the following functions
        - `from pprint import pprint; pprint(cleaner.default_rename_columns_dict)`
          Use the printed dictionary as `keep_and_rename_columns` argument
        - cleaner.profile_raw_csv()
          This will compute and write an html report on the dataset.
          Use it to proceed with columns cleaning
          - Remove useless columns from `keep_and_rename_columns`
          - Create or modify columns with `assign_columns_with_functions`

    """

    def __init__(self, input_csv_name,
                 input_separator=DEFAULT_SEPARATOR,
                 input_csv_dir=ROOTED_RAW_DATA_DIR,
                 output_csv_name=None,
                 output_separator=DEFAULT_SEPARATOR,
                 output_csv_dir=ROOTED_CLEANED_DATA_DIR,
                 chunk_size=CSV_CHUNK_SIZE,
                 keep_and_rename_columns=None,
                 assign_columns_with_functions=None,
                 piped_transformations=None,
                 additional_columns_from_piped_transformations=None,
                 columns_to_drop=None,
                 validators=None,
                 columns_unique=None,
                 columns_not_null=None,
                 ):
        self.input_csv_name = input_csv_name
        self.input_separator = input_separator
        self.input_csv_path = os.path.join(input_csv_dir, self.input_csv_name)

        self.output_csv_name = output_csv_name or input_csv_name
        self.output_separator = output_separator
        self.output_csv_path = os.path.join(output_csv_dir, self.output_csv_name)

        self.chunk_size = chunk_size
        self.keep_and_rename_columns = keep_and_rename_columns
        self.assign_columns_with_functions = assign_columns_with_functions or dict()
        self.piped_transformations = piped_transformations or list()
        self.additional_columns_from_piped_transformations = additional_columns_from_piped_transformations or list()
        self.columns_to_drop = columns_to_drop or list()

        self.validators = validators or list()
        self.columns_unique = columns_unique or list()
        self.columns_not_null = columns_not_null or list()

    @staticmethod
    def profile_csv(csv_path, separator, profile_report_dir):
        """ Create an html report on csv

        Locally patch pandas_profiling/report.py to avoid computation of correlation, with comment on following lines :

        # pearson_matrix = plot.correlation_matrix(stats_object['correlations']['pearson'], 'Pearson')
        # spearman_matrix = plot.correlation_matrix(stats_object['correlations']['spearman'], 'Spearman')
        # correlations_html = templates.template('correlations').render(
        #     values={'pearson_matrix': pearson_matrix, 'spearman_matrix': spearman_matrix})
        # …
        #     'correlation_html': correlations_html

        """

        report_path = os.path.join(profile_report_dir, prefix(os.path.basename(csv_path)) + '.html')
        print("Profiling '{}'…".format(csv_path))

        df = pd.read_csv(csv_path, sep=separator, low_memory=False, nrows=ROWS_LIMIT)
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            profile_report = pandas_profiling.ProfileReport(df, check_correlation=False)
        print("Writing report in file '{path}'".format(path=os.path.abspath(report_path)))
        profile_report.to_file(report_path)

    def profile_raw_csv(self):
        self.profile_csv(self.input_csv_path, self.input_separator, RAW_PROFILE_REPORT_DIR)

    def profile_cleaned_csv(self):
        self.profile_csv(self.output_csv_path, self.output_separator, CLEANED_PROFILE_REPORT_DIR)

    def print_base_rename_columns_dict(self):
        """ Helper function to write a new cleaner from a raw csv file. """

        print("Print a default rename group dict for file '{}'".format(self.input_csv_path))
        print(json.dumps(self.default_rename_columns_dict, sort_keys=True, indent=4, ensure_ascii=False))

    @property
    def default_rename_columns_dict(self):
        return {column: column for column in self.input_csv_columns}

    @property
    def input_csv_columns(self):
        return get_csv_columns(self.input_csv_path, separator=self.input_separator)

    @timeit
    def run(self):
        print("Cleaning '{}' by chunks of {} rows.".format(self.input_csv_path, self.chunk_size))
        self.keep_and_rename_columns = self.keep_and_rename_columns or self.default_rename_columns_dict
        print("Cleaned file will have the following columns : {}".format(self._cleaned_csv_columns))
        write_df_chunk_iterator(self.output_csv_path, self.output_df_chunk_iterator, self.output_separator)
        print("Cleaned file is written to '{}".format(self.output_csv_path))

    def reset(self):
        print("Removing", self.output_csv_path)
        os.remove(self.output_csv_path)

    def output_df_chunk_iterator(self):
        number_of_cleaned_lines = 0
        for raw_df_chunk in read_csv_by_chunk(self.input_csv_path,
                                              chunk_size=self.chunk_size,
                                              separator=self.input_separator,
                                              columns_to_read=self.keep_and_rename_columns.keys()):
            number_of_cleaned_lines += len(raw_df_chunk)
            cleaned_df_chunk = raw_df_chunk.rename(columns=self.keep_and_rename_columns)
            for column, column_function in self.assign_columns_with_functions.items():
                cleaned_df_chunk = cleaned_df_chunk.assign(**{column: column_function})

            for transformation in self.piped_transformations:
                cleaned_df_chunk = cleaned_df_chunk.pipe(transformation)

            cleaned_df_chunk = cleaned_df_chunk.drop(columns=self.columns_to_drop)
            self._validate(cleaned_df_chunk)
            print("  {} lines cleaned.".format(number_of_cleaned_lines))
            yield cleaned_df_chunk

    @property
    def _cleaned_csv_columns(self):
        columns = set(self.keep_and_rename_columns.values())
        columns |= self.assign_columns_with_functions.keys()
        columns |= set(self.additional_columns_from_piped_transformations)
        columns -= set(self.columns_to_drop)
        return sorted(list(columns))

    def _validate(self, df):
        validators = [
            validate.get_checker_columns_set(set(self._cleaned_csv_columns)),
            validate.get_checker_columns_are_unique(self.columns_unique),
            validate.get_checker_columns_have_no_null(self.columns_not_null)
        ]

        for validator in validators + self.validators:
            validator(df)

