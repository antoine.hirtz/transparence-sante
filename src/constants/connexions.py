from src.utils.utils import is_running_in_docker

LOCALHOST = 'localhost'
POSTGRES_USER = 'postgres'
DOCKER_POSTGRES_HOST = 'postgres'
DOCKER_METABASE_HOST = 'metabase'


def get_default_postgres_host():
    if is_running_in_docker():
        return DOCKER_POSTGRES_HOST
    else:
        return LOCALHOST


def get_default_metabase_host():
    if is_running_in_docker():
        return DOCKER_METABASE_HOST
    else:
        return LOCALHOST
