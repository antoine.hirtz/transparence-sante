# Common to all files
ID = 'id'

# Common to Health directory & 3 declarations files
PROFESSIONAL_ID = 'identifiant'
NAME = 'nom'
SURNAME = 'prénom'
NAME_SURNAME = NAME + '_' + SURNAME
PROFESSION = "profession"
SPECIALTY = "specialité"
ID_TYPE = 'type_identifiant'


# Specific to Health directory
MINIMAL_PREFIX_SEARCH = 'prefix_search_string_minimal'
FULL_PREFIX_SEARCH = 'prefix_search_string_full'
PROFESSIONAL_CATEGORY = "categorie_professionnelle"
TITLE = "civilite"
PROFESSIONAL_TITLE = "civilite_exercice"
EMPLOYMENT_STATUS = "mode_exercice"
LOCATION_SIRET_ID = "siret_site"
LOCATION_FINESS_ID = "siren_site"
LOCATION_SIREN_ID = "finess_site"
LEGAL_ESTABLISHMENT_FINESS_ID = "finess_etablissement_juridique"
ORGANIZATION_ID = "identifiant_organisation"
LOCATION_CORPORATE_NAME = "raison_sociale_site"
LOCATION_COMMERCIAL_NAME = "enseigne_commerciale_site"
TOWN = "commune"

NUMBER_OF_DECLARATIONS = 'nombre_liens'
TOTAL_AMOUNT = "montant_total"
NUMBER_CONTRACT_WITHOUT_AMOUNT = "nombre_convention_sans_montant"

LATITUDE = "latitude"
LONGITUDE = "longitude"


ROAD_NUMBER = 'numero_voie'
REPETITION_INDEX = 'indice_repetition_voie'
STREET_TYPE = 'type_voie'
STREET = 'voie'
RECIPIENT_COMPLEMENTARY_INFO = 'complement_destinataire'
GEOGRAPHICAL_POINT_COMPEMENTARY_INFO = 'complement_point_geographique'
DISTRIBUTION_MENTION = 'mention_distribution'
CEDEX_CODE = 'cedex'
TOWN_CODE = 'code_commune'
PHONE = 'telephone'
PHONE_2 = 'telephone_2'
FAX = 'telecopie'
EMAIL = 'email'

# Common to Company directory & 3 declarations files
COMPANY_ID = "identifiant_entreprise"
COMPANY_NAME = "denomination_sociale"

# Specific to Company to Groupe association
COMPANY_GROUP = "groupe"

# Specific to Company directory file
COUNTRY = "pays"
SECTOR = "secteur"
ADDRESS = "adresse"
POSTAL_CODE = "code_postal"
CITY = "ville"
COUNTRY_CODE = "code_pays"

# Specific to 3 declarations files
# - common declaration cleaner

BENEFICIARY_CATEGORY = 'catégorie_beneficiaire'
SUBSIDIARY_COMPANY = 'filiale_déclarante'
COMPANY = "entreprise_émmetrice"
DECLARATION_TYPE = 'type_declaration'
IS_AMOUNT_MASKED = 'montant_masque'
BENEFICIARY_ORGANIZATION_NAME = 'structure_bénéficiaire'
YEAR = "annee"

# - specific to each declaration cleaner, but present in all 3 declaration files
LINE_ID = 'ligne_identifiant'
DECLARATION_ID = 'declaration_id'
DATE = 'date'
AMOUNT = 'montant_ttc'
CONVENTION_ID = 'identifiant_convention'
DETAIL = 'detail'
PRECISE_CATEGORY = 'categorie_precise'
GENERAL_CATEGORY = 'categorie_generale'

# - specific to convention
RAW_COLUMN_CONVENTION_OBJECT = 'conv_objet'
RAW_COLUMN_CONV_OBJECT_OTHER = 'conv_objet_autre'
RAW_EVENT_NAME = 'conv_manifestation_nom'
RAW_EVENT_DATE = 'conv_manifestation_date'
RAW_EVENT_PLACE = 'conv_manifestation_lieu'
RAW_EVENT_ORGANIZER = 'conv_manifestation_organisateur'
RAW_COLUMNS_CONVENTION = [
    RAW_COLUMN_CONVENTION_OBJECT,
    RAW_COLUMN_CONV_OBJECT_OTHER,
    RAW_EVENT_NAME,
    RAW_EVENT_DATE,
    RAW_EVENT_PLACE,
    RAW_EVENT_ORGANIZER,
]

NUMBER_LINKED_REMUNERATIONS = 'nombre_remunerations_liees'
NUMBER_LINKED_ADVANTAGES = 'nombre_avantages_lies'
TOTAL_AMOUNT_LINKED_REMUNERATIONS = 'montant_total_remunerations_liees'
TOTAL_AMOUNT_LINKED_ADVANTAGES = 'montant_total_avantages_lies'
TOTAL_AMOUNT_LINKED_DECLARATIONS = 'montant_total_declarations_liees'
TOTAL_AMOUNT_CONVENTION = 'montant_total_convention'
DECLARED_AMOUNT_CONVENTION = 'montant_declare_convention'
