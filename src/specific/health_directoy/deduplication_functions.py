import pandas as pd

from src.constants import column
from src.generic.validate import ValidationError
from src.utils.utils import timeit

STUDENT = 'Etudiant'
MAX_STRING = 'z' * 10


@timeit
def drop_multiple_town(df):
    df[column.TOWN] = df[column.TOWN].fillna(MAX_STRING)
    df = drop_rows_to_deduplicate_column(df, column.TOWN)
    df[column.TOWN] = df[column.TOWN].replace(MAX_STRING, pd.np.nan)
    return df


def drop_multiple_specialty(df):
    sorted_value = list(df[column.SPECIALTY].value_counts().sort_values().index)  # Rarest fist
    transform_column_to_sorted_category(df, column.SPECIALTY, sorted_value)
    df = drop_rows_to_deduplicate_column(df, column.SPECIALTY)
    transform_column_back_to_str(df, column.SPECIALTY)
    return df


def drop_multiple_professional_category(df):
    sorted_professional_category = [
        STUDENT,
        'Civil',
        'Militaire',
    ]
    transform_column_to_sorted_category(df, column.PROFESSIONAL_CATEGORY, sorted_professional_category)
    df = drop_rows_to_deduplicate_column(df, column.PROFESSIONAL_CATEGORY)
    transform_column_back_to_str(df, column.PROFESSIONAL_CATEGORY)

    return df


def drop_multiple_profession(df):
    sorted_profession = [
        'Médecin',
        'Chirurgien-Dentiste',
        'Pharmacien',
        'Sage-Femme',
        'Masseur-Kinésithérapeute',
        'Infirmier',
        'Opticien-Lunetier',
        'Technicien de laboratoire médical',
        'Manipulateur ERM',
        'Orthophoniste',
        'Psychomotricien',
        'Diététicien',
        'Pédicure-Podologue',
        'Ergothérapeute',
        'Orthoptiste',
        'Audioprothésiste',
        'Orthopédiste-Orthésiste',
        'Orthoprothésiste',
        'Podo-Orthésiste',
        'Epithésiste',
        'Oculariste'
    ]
    transform_column_to_sorted_category(df, column.PROFESSION, sorted_profession)
    df = drop_rows_to_deduplicate_column(df, column.PROFESSION)
    transform_column_back_to_str(df, column.PROFESSION)
    return df


def drop_rows_to_deduplicate_column(df, column_name):
    mask_multiple_values_for_id = get_mask_rows_with_multiple_values_for_same_id(df, column_name)
    rows_to_drop_to_deduplicate = (df[mask_multiple_values_for_id]
                                   .groupby(column.PROFESSIONAL_ID)
                                   .apply(list_rows_to_drop_for_one_professional_id_to_deduplicate_column, column_name)
                                   .sum()
                                   )
    print("Dropping {} rows to deduplicate column {}.".format(len(rows_to_drop_to_deduplicate), column_name))
    return df.drop(rows_to_drop_to_deduplicate)


def transform_column_to_sorted_category(df, column_name, sorted_values):
    missing_sorted_values = set(df[column_name].dropna().unique()) - set(sorted_values)
    if missing_sorted_values:
        raise ValidationError("Some values of column {} are missing from sorted values : {}"
                              .format(column_name, missing_sorted_values))
    df[column_name] = pd.Categorical(df[column_name], categories=sorted_values, ordered=True)


def transform_column_back_to_str(df, column_name):
    df[column_name] = df[column_name].astype(str).replace('nan', pd.np.nan)


def get_mask_rows_with_multiple_values_for_same_id(df, column_name):
    nb_values_by_id = df.groupby(column.PROFESSIONAL_ID)[column_name].nunique(dropna=False)
    set_id_multiple_values = set(nb_values_by_id[nb_values_by_id > 1].index)
    mask_multiple_values_for_id = df[column.PROFESSIONAL_ID].isin(set_id_multiple_values)
    return mask_multiple_values_for_id


def list_rows_to_drop_for_one_professional_id_to_deduplicate_column(df_id, column_name):
    values = df_id[column_name].unique()
    if column_name != column.TOWN and len(values) != 2:
        print("WARNING: input dataframe should have exactly 2 values for column '{}'.".format(column_name))

    # Keep the lowest value, unless for professional_category, for student and civilian/miliatry with only 1 profession
    if column_name == column.PROFESSIONAL_CATEGORY and STUDENT in values and df_id.profession.nunique() == 1:
        value_to_keep = values.max()  # not student
    else:
        value_to_keep = values.min()
    rows_to_drop = df_id[column_name] != value_to_keep
    return list(df_id[rows_to_drop].index)


def drop_duplicates(df):
    print("Dropping {} duplicated rows".format(df.duplicated().sum()))
    return df.drop_duplicates()
