from typing import List, Tuple

import pandas as pd

from src.constants import column
from src.constants.column import FULL_PREFIX_SEARCH, MINIMAL_PREFIX_SEARCH


def create_full_prefix_search_string(df):
    print("Create a full unique prefix search string, for each professional, starting from '{}'"
          .format(column.NAME_SURNAME))

    df[FULL_PREFIX_SEARCH] = concat_str_columns_from_df(df, column.NAME_SURNAME, [
        (column.PROFESSION, ', '),
        (column.SPECIALTY, ', '),
        (column.TOWN, ', à '),
        (column.ID_TYPE, ', n°'),
        (column.PROFESSIONAL_ID, ' '),
    ])
    return df


def create_minimal_prefix_search_string(df):
    print("Create a minimal unique prefix search string, for each professional, starting from '{}'"
          .format(column.NAME_SURNAME))

    df[MINIMAL_PREFIX_SEARCH] = df[column.NAME_SURNAME]
    concat_other_column_to_prefix_search(df, MINIMAL_PREFIX_SEARCH, column.PROFESSION)
    concat_other_column_to_prefix_search(df, MINIMAL_PREFIX_SEARCH, column.SPECIALTY)
    concat_other_column_to_prefix_search(df, MINIMAL_PREFIX_SEARCH, column.TOWN, sep=', à ')
    concat_other_column_to_prefix_search(df, MINIMAL_PREFIX_SEARCH, column.PROFESSIONAL_ID, sep=', n°',
                                         column_to_select_duplicates=column.NAME_SURNAME)
    return df


def concat_str_columns_from_df(df: pd.DataFrame, first_column: str,
                               column_sep_tuples: List[Tuple[str, str]]) -> pd.Series:
    result_series = df[first_column]
    for column_name, sep in column_sep_tuples:
        result_series = concat_two_str_series(result_series, df[column_name], sep=sep)
    return result_series


def concat_two_str_series(str_series_1: pd.Series, str_series_2: pd.Series, sep: str):
    return (str_series_1.fillna('')
            .str.cat(str_series_2.fillna('').values, sep=sep)
            .apply(rchop, args=(sep,))
            .str.strip()
            )


def rchop(s, ending):
    if s.endswith(ending):
        return s[:-len(ending)]
    return s


def concat_other_column_to_prefix_search(df, column_to_extend, column_to_add, sep=', ',
                                         column_to_select_duplicates=None):
    column_to_select_duplicates = column_to_select_duplicates or column_to_extend

    mask_duplicated = df[column_to_select_duplicates].duplicated(keep=False)
    print("Concat column '{}' to {} rows to deduplicate column '{}'"
          .format(column_to_add, mask_duplicated.sum(), column_to_select_duplicates))
    df_duplicated = df.loc[mask_duplicated]
    s_new = concat_two_str_series(df_duplicated[column_to_extend], df_duplicated[column_to_add], sep=sep)
    df[column_to_extend].update(s_new)
