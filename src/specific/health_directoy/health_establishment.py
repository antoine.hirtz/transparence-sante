from src.constants import column, tables
from src.constants import csv
from src.generic.cleaner import Cleaner
from src.generic.ddl.index import IndexDDL, SearchIndexDDL
from src.generic.uploader import Uploader
from src.specific.health_directoy import deduplication_functions
from src.specific.health_directoy.search_string import concat_str_columns_from_df

from src.specific.types import index_list


def build_address(df):
    return concat_str_columns_from_df(
        df,
        first_column=column.ROAD_NUMBER,
        column_sep_tuples=[
            (column.REPETITION_INDEX, ' '),
            (column.STREET_TYPE, ' '),
            (column.STREET, ' '),
            (column.POSTAL_CODE, ' '),
            (column.TOWN, ' '),
        ]
    )


cleaner = Cleaner(
    input_csv_name=csv.HEALTH_PROFESSIONAL_ACTIVITY_RAW_CSV,
    output_csv_name=csv.HEALTH_ESTABLISHMENT_CSV,
    input_separator='|',
    chunk_size=10 ** 7,  # We need to clean the whole data set in one go in order to remove duplicates
    keep_and_rename_columns={
        # STRUCTURE
        'Identifiant technique de la structure': column.ORGANIZATION_ID,
        'Numéro SIRET site': column.LOCATION_SIRET_ID,
        'Numéro FINESS site': column.LOCATION_FINESS_ID,
        'Numéro SIREN site': column.LOCATION_SIREN_ID,
        'Numéro FINESS établissement juridique': column.LEGAL_ESTABLISHMENT_FINESS_ID,
        'Raison sociale site': column.LOCATION_CORPORATE_NAME,
        'Enseigne commerciale site': column.LOCATION_COMMERCIAL_NAME,
        # 'Ancien identifiant de la structure': 'Ancien identifiant de la structure',

        # COORDONNEES STRUCTURE
        'Numéro Voie (coord. structure)': column.ROAD_NUMBER,
        'Indice répétition voie (coord. structure)': column.REPETITION_INDEX,
        'Libellé type de voie (coord. structure)': column.STREET_TYPE,
        'Libellé Voie (coord. structure)': column.STREET,
        'Complément destinataire (coord. structure)': column.RECIPIENT_COMPLEMENTARY_INFO,
        'Complément point géographique (coord. structure)': column.GEOGRAPHICAL_POINT_COMPEMENTARY_INFO,
        'Mention distribution (coord. structure)': column.DISTRIBUTION_MENTION,
        'Bureau cedex (coord. structure)': column.CEDEX_CODE,
        'Code postal (coord. structure)': column.POSTAL_CODE,
        'Code commune (coord. structure)': column.TOWN_CODE,
        'Libellé commune (coord. structure)': column.TOWN,
        'Libellé pays (coord. structure)': column.COUNTRY,
        'Téléphone (coord. structure)': column.PHONE,
        'Téléphone 2 (coord. structure)': column.PHONE_2,
        'Télécopie (coord. structure)': column.FAX,
        'Adresse e-mail (coord. structure)': column.EMAIL,

        # "Autorité d'enregistrement": "Autorité d'enregistrement",
    },
    assign_columns_with_functions={
        column.COUNTRY: lambda df: df[column.COUNTRY].fillna('France'),
        column.ADDRESS: build_address,
    },
    piped_transformations=[
        lambda df: df.dropna(subset=[column.ORGANIZATION_ID]),
        deduplication_functions.drop_duplicates,
    ],
    columns_unique=[column.ORGANIZATION_ID],
    columns_not_null=[column.ORGANIZATION_ID],
)

uploader = Uploader(csv.HEALTH_ESTABLISHMENT_CSV, table_name=tables.HEALTH_ESTABLISHMENT_DIRECTORY)

SEARCH_COLUMNS = [column.LOCATION_CORPORATE_NAME, column.LOCATION_COMMERCIAL_NAME]
indices: index_list = [
    SearchIndexDDL(tables.HEALTH_ESTABLISHMENT_DIRECTORY, column_name)
    for column_name in SEARCH_COLUMNS
]

INDEX_COLUMNS = [
    column.LOCATION_FINESS_ID,
    column.LOCATION_SIREN_ID,
    column.LOCATION_SIRET_ID,
    # "latitude",
    # "longitude"
]
indices += [
    IndexDDL(tables.HEALTH_ESTABLISHMENT_DIRECTORY, column_name)
    for column_name in INDEX_COLUMNS
]
indices.append(IndexDDL(tables.HEALTH_ESTABLISHMENT_DIRECTORY, column.ORGANIZATION_ID, unique=True))

if __name__ == '__main__':
    cleaner.run()
    cleaner.profile_cleaned_csv()
    uploader.reset()
    uploader.run()
