from src.constants.csv import COMPANY_GROUP_URL, COMPANY_GROUP_CSV
from src.generic.downloader import Downloader

company_group_downloader = Downloader(COMPANY_GROUP_URL, COMPANY_GROUP_CSV)

if __name__ == '__main__':
    company_group_downloader.run()
