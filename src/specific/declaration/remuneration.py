from src.constants import column
from src.constants.categorical import REMUNERATION, CONVENTION
from src.constants.csv import REMUNERATION_CSV
from src.specific.declaration.common import DeclarationCleaner, DeclarationUploader
from src.specific.declaration.functions import get_declaration_id_builder

cleaner = DeclarationCleaner(
    input_csv_name=REMUNERATION_CSV,
    keep_and_rename_columns={
        'remu_date': column.DATE,
        'remu_montant_ttc': column.AMOUNT,
        'remu_convention_liee': column.CONVENTION_ID
    },
    assign_columns_with_functions={
        column.DECLARATION_TYPE: REMUNERATION,
        column.DETAIL: None,
        column.PRECISE_CATEGORY: None,
        column.GENERAL_CATEGORY: None,
        column.DECLARATION_ID: get_declaration_id_builder(REMUNERATION, column.LINE_ID),
        column.CONVENTION_ID: get_declaration_id_builder(CONVENTION, column.CONVENTION_ID)
    },
    columns_to_drop=list()
)
uploader = DeclarationUploader(REMUNERATION_CSV)

if __name__ == '__main__':
    # cleaner.run()
    cleaner.profile_cleaned_csv()
