from collections import OrderedDict

from src.constants import column, tables
from src.constants.column import DATE, AMOUNT
from src.constants.csv import TRANSPARENCE_SANTE_URL, TRANSPARENCE_SANTE_ZIP_NAME, ETALAB_CSV_LIST
from src.generic.cleaner import Cleaner
from src.generic.ddl.foreign_key import ForeignKeyDDL
from src.generic.ddl.index import IndexDDL, SearchIndexDDL
from src.generic.ddl.materialized_view import AggregatedMaterializedViewDDL
from src.generic.downloader import Downloader, Extractor
from src.generic.uploader import Uploader
from src.specific.company import parent_company
from src.specific.declaration.functions import concat_name_surname, corrected_date, validate_year_is_in_correct_range, \
    normalize_surname, normalize_name
from src.utils.utils import merge_dict

downloader = Downloader(TRANSPARENCE_SANTE_URL, TRANSPARENCE_SANTE_ZIP_NAME)
extractor_list = [Extractor(TRANSPARENCE_SANTE_ZIP_NAME, csv_name) for csv_name in ETALAB_CSV_LIST]

RENAME_COLUMNS = {
    'ligne_identifiant': column.LINE_ID,
    'benef_nom': column.NAME,
    'benef_prenom': column.SURNAME,
    'benef_identifiant_valeur': column.PROFESSIONAL_ID,
    'entreprise_identifiant': column.COMPANY_ID,
    'denomination_sociale': column.SUBSIDIARY_COMPANY,
    'categorie': column.BENEFICIARY_CATEGORY,
    'identifiant_type': column.ID_TYPE,
    'benef_speicalite_libelle': column.SPECIALTY,
    'qualite': column.PROFESSION,
    'benef_denomination_sociale': column.BENEFICIARY_ORGANIZATION_NAME,
}

ASSIGN_BEFORE = OrderedDict([
    (column.NAME, normalize_name),
    (column.SURNAME, normalize_surname),
    (column.NAME_SURNAME, concat_name_surname),
    (column.COMPANY, parent_company),
])

ASSIGN_AFTER = {
    column.DATE: corrected_date,
    column.YEAR: lambda df: df[column.DATE].str[6:]
}

COLUMNS_TO_DROP = [column.LINE_ID]

VALIDATORS = [validate_year_is_in_correct_range]

DECLARATION_COLUMN_TYPE_DICT = {DATE: 'DATE', AMOUNT: 'NUMERIC'}

DECLARATION_SEARCH_COLUMNS = [
    column.NAME_SURNAME,
    column.PROFESSIONAL_ID,
    # column.SUBSIDIARY_COMPANY,
    column.COMPANY,
    column.BENEFICIARY_ORGANIZATION_NAME,
    # column.BENEFICIARY_CATEGORY,
    # column.SPECIALTY
    # column.CONVENTION_ID
]

DECLARATION_AGGREGATE_VALUE_BY_GROUP = [
    (column.AMOUNT, column.NAME_SURNAME),
    (column.AMOUNT, column.SUBSIDIARY_COMPANY),
    (column.AMOUNT, column.COMPANY),
    (column.AMOUNT, [column.SPECIALTY, column.YEAR]),
]


class DeclarationCleaner(Cleaner):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keep_and_rename_columns = merge_dict([RENAME_COLUMNS, self.keep_and_rename_columns])
        self.assign_columns_with_functions = merge_dict([ASSIGN_BEFORE, self.assign_columns_with_functions,
                                                         ASSIGN_AFTER])
        self.columns_to_drop = COLUMNS_TO_DROP + self.columns_to_drop
        self.validators = VALIDATORS + self.validators


class DeclarationUploader(Uploader):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.columns_with_non_default_type = DECLARATION_COLUMN_TYPE_DICT


declaration_tables = [tables.DECLARATION_ADVANTAGE, tables.DECLARATION_CONVENTION, tables.DECLARATION_REMUNERATION]

indices = [
    SearchIndexDDL(table, column_name)
    for column_name in DECLARATION_SEARCH_COLUMNS
    for table in declaration_tables
]

indices += [
    IndexDDL(table, column.COMPANY_ID)
    for table in declaration_tables
]

indices += [
    IndexDDL(table, column.DECLARATION_ID, unique=True)
    for table in declaration_tables
]

foreign_keys = [
    ForeignKeyDDL(table, column.COMPANY_ID, tables.COMPANY_DIRECTORY)
    for table in declaration_tables
]

materialized_views = [
    AggregatedMaterializedViewDDL(tables.DECLARATION, value, column)
    for value, column in DECLARATION_AGGREGATE_VALUE_BY_GROUP
]

if __name__ == '__main__':
    for ddl in materialized_views:
        ddl.run()
