import os

import numpy as np
import pandas as pd

from settings import ROWS_LIMIT
from src.constants import column
from src.constants import csv
from src.constants.directories import ROOTED_CLEANED_DATA_DIR, ROOTED_JOINED_DATA_DIR
from src.generic.cleaner import Cleaner
from src.generic.uploader import Uploader
from src.specific.declaration.common import DECLARATION_COLUMN_TYPE_DICT

CLEANED_REMUNERATION_PATH = os.path.join(ROOTED_CLEANED_DATA_DIR, csv.REMUNERATION_CSV)
CLEANED_ADVANTAGE_PATH = os.path.join(ROOTED_CLEANED_DATA_DIR, csv.ADVANTAGE_CSV)

ADDITIONAL_NUMERICAL_COLUMNS = [
    column.NUMBER_LINKED_REMUNERATIONS,
    column.NUMBER_LINKED_ADVANTAGES,
    column.TOTAL_AMOUNT_LINKED_REMUNERATIONS,
    column.TOTAL_AMOUNT_LINKED_ADVANTAGES,
    column.TOTAL_AMOUNT_CONVENTION,
    column.DECLARED_AMOUNT_CONVENTION,
]
ADDITIONAL_BOOLEAN_COLUMNS = [column.IS_AMOUNT_MASKED]

CONVENTION_COLUMN_DICT = DECLARATION_COLUMN_TYPE_DICT.copy()
for additional_column in ADDITIONAL_NUMERICAL_COLUMNS:
    CONVENTION_COLUMN_DICT[additional_column] = 'NUMERIC'
for additional_column in ADDITIONAL_BOOLEAN_COLUMNS:
    CONVENTION_COLUMN_DICT[additional_column] = 'BOOLEAN'

df_aggregated_linked_remuneration = pd.DataFrame()
df_aggregated_linked_advantages = pd.DataFrame()


def build_aggregated_linked_declarations(detailed_declaration_csv_path):
    df_detailed_linked_declarations = pd.read_csv(
        detailed_declaration_csv_path,
        sep=csv.DEFAULT_SEPARATOR,
        usecols=[column.CONVENTION_ID, column.AMOUNT],
        nrows=ROWS_LIMIT
    )
    return (df_detailed_linked_declarations
            .dropna(subset=[column.CONVENTION_ID])
            .groupby(column.CONVENTION_ID)[column.AMOUNT]
            .agg([np.sum, np.size])
            )


def get_aggregated_linked_remuneration():
    global df_aggregated_linked_remuneration
    if len(df_aggregated_linked_remuneration):
        return df_aggregated_linked_remuneration
    else:
        df_aggregated_linked_remuneration = build_aggregated_linked_declarations(CLEANED_REMUNERATION_PATH)
        df_aggregated_linked_remuneration.columns = [column.TOTAL_AMOUNT_LINKED_REMUNERATIONS,
                                                     column.NUMBER_LINKED_REMUNERATIONS]
        return df_aggregated_linked_remuneration


def get_aggregated_linked_advantages():
    global df_aggregated_linked_advantages
    if len(df_aggregated_linked_advantages):
        return df_aggregated_linked_advantages
    else:
        df_aggregated_linked_advantages = build_aggregated_linked_declarations(CLEANED_ADVANTAGE_PATH)
        df_aggregated_linked_advantages.columns = [column.TOTAL_AMOUNT_LINKED_ADVANTAGES,
                                                   column.NUMBER_LINKED_ADVANTAGES]
        return df_aggregated_linked_advantages


def add_linked_declarations(df_convention_chunk, df_aggregated_linked_declarations):
    df_convention_chunk_with_agg_data = df_convention_chunk.merge(
        df_aggregated_linked_declarations,
        how='left',
        left_on=column.CONVENTION_ID,
        right_index=True,
        validate='one_to_one'
    )
    agg_columns = df_aggregated_linked_declarations.columns
    df_convention_chunk_with_agg_data[agg_columns] = df_convention_chunk_with_agg_data[agg_columns].fillna(0)
    return df_convention_chunk_with_agg_data


def add_linked_remunerations_and_advantage(df_convention_chunk):
    return (df_convention_chunk
            .pipe(add_linked_declarations, get_aggregated_linked_remuneration())
            .pipe(add_linked_declarations, get_aggregated_linked_advantages())
            )


def compute_amounts(df_convention_chunk):
    df_conv = df_convention_chunk
    df_conv[column.TOTAL_AMOUNT_LINKED_DECLARATIONS] = (df_conv[column.TOTAL_AMOUNT_LINKED_REMUNERATIONS] +
                                                        df_conv[column.TOTAL_AMOUNT_LINKED_ADVANTAGES])
    df_conv[column.DECLARED_AMOUNT_CONVENTION] = df_conv[column.AMOUNT].map(float)
    df_conv[column.TOTAL_AMOUNT_CONVENTION] = df_conv[
        [column.DECLARED_AMOUNT_CONVENTION, column.TOTAL_AMOUNT_LINKED_DECLARATIONS]]\
        .fillna(0).max(axis='columns')
    df_conv[column.AMOUNT] = (df_conv[column.TOTAL_AMOUNT_CONVENTION] -
                              df_conv[column.TOTAL_AMOUNT_LINKED_DECLARATIONS].fillna(0))
    df_conv[column.IS_AMOUNT_MASKED] = df_conv[column.TOTAL_AMOUNT_CONVENTION] == 0
    del df_conv[column.TOTAL_AMOUNT_LINKED_DECLARATIONS]
    return df_conv


cleaner = Cleaner(
    input_csv_name=csv.CONVENTION_CSV,
    input_csv_dir=ROOTED_CLEANED_DATA_DIR,
    output_csv_dir=ROOTED_JOINED_DATA_DIR,
    piped_transformations=[add_linked_remunerations_and_advantage, compute_amounts],
    additional_columns_from_piped_transformations=ADDITIONAL_NUMERICAL_COLUMNS + ADDITIONAL_BOOLEAN_COLUMNS
)

uploader = Uploader(
    csv.CONVENTION_CSV,
    csv_directory=ROOTED_JOINED_DATA_DIR,
    columns_with_non_default_type=CONVENTION_COLUMN_DICT
)

if __name__ == '__main__':
    cleaner.run()
