from collections import OrderedDict

from src.constants import column
from src.constants.categorical import ADVANTAGE, CONVENTION
from src.constants.csv import ADVANTAGE_CSV
from src.specific.declaration.common import DeclarationCleaner, DeclarationUploader
from src.specific.declaration.functions import get_declaration_id_builder
from src.specific.map_to_ontology import TMP_CATEGORY, get_category, get_precise_category, get_general_category
from src.specific.normalize_functions import normalize_detail

cleaner = DeclarationCleaner(
    input_csv_name=ADVANTAGE_CSV,
    keep_and_rename_columns={
        'avant_date_signature': column.DATE,
        'avant_montant_ttc': column.AMOUNT,
        'avant_convention_lie': column.CONVENTION_ID,
        'avant_nature': column.DETAIL,
    },
    assign_columns_with_functions=OrderedDict([
        (column.DECLARATION_TYPE, ADVANTAGE),
        (column.DETAIL, lambda df: df[column.DETAIL].pipe(normalize_detail)),
        (TMP_CATEGORY, get_category),
        (column.PRECISE_CATEGORY, get_precise_category),
        (column.GENERAL_CATEGORY, get_general_category),
        (column.DECLARATION_ID, get_declaration_id_builder(ADVANTAGE, column.LINE_ID)),
        (column.CONVENTION_ID, get_declaration_id_builder(CONVENTION, column.CONVENTION_ID)),
    ]),
    columns_to_drop=[TMP_CATEGORY]
)
uploader = DeclarationUploader(ADVANTAGE_CSV)

if __name__ == '__main__':
    # cleaner.run()
    cleaner.profile_cleaned_csv()
