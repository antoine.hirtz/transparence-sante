from typing import List, Union

from src.generic.ddl.index import IndexDDL, SearchIndexDDL

index_list = List[Union[IndexDDL, SearchIndexDDL]]