import string

import pandas as pd

from src.constants import column
from src.constants.ontology import NO_INFORMATION
from src.generic.cleaner_functions import get_non_authorized_chars_remover, replace_accents, remove_extra_space

OTHER = 'AUTRE'
GENERAL_MEDICINE = "Médecine Générale"
CITIES_WITH_DISTRICT_NUMBERS = ['Paris', 'Marseille', 'Lyon']


def normalize_string_series(series, authorized_chars=string.ascii_letters):
    return (series
            .fillna('').map(str)
            .map(replace_accents)
            .map(get_non_authorized_chars_remover(authorized_chars))
            .map(remove_extra_space)
            )


def normalize_detail(detail_series):
    return (detail_series
            .str.upper()
            .str.replace(OTHER, ' ')
            .pipe(normalize_string_series, string.ascii_letters + string.digits + '/')
            .replace('', NO_INFORMATION)
            )


def normalize_surname(df):
    return (df[column.SURNAME]
            .pipe(normalize_string_series)
            .str.title()
            )


def normalize_name(df):
    return (df[column.NAME]
            .pipe(normalize_string_series)
            .str.upper()
            )


def normalize_city(city_series):
    non_ascii_or_digits_remover = get_non_authorized_chars_remover(string.ascii_letters + string.digits)
    return (city_series
            .fillna('').map(str)
            .map(replace_accents)
            .map(non_ascii_or_digits_remover)
            .str.title()
            .str.replace('St', 'Saint')
            .map(normalize_cities_with_district_number)
            .map(remove_extra_space)
            .replace('', pd.np.nan)
            )


def normalize_specialty(df):
    mapping_specialties = {
        "Spécialiste en Médecine Générale": GENERAL_MEDICINE,
        "Qualifié en Médecine Générale": GENERAL_MEDICINE,
        "Médecine Générale": GENERAL_MEDICINE,
    }
    return df[column.SPECIALTY].map(
        lambda x: mapping_specialties[x] if x in mapping_specialties else x
    )


def normalize_cities_with_district_number(city):
    is_city_with_digits = any(char.isdigit() for char in city)
    if is_city_with_digits:
        for city_with_district in CITIES_WITH_DISTRICT_NUMBERS:
            if city.startswith(city_with_district):
                return "{} {}".format(city_with_district, extract_number_from_city_name(city))
        raise ValueError(
            "City '{}' contains a number, but is not one of the cities with a district.".format(city))
    else:
        return city


def extract_number_from_city_name(city):
    non_digits_remover = get_non_authorized_chars_remover(string.digits)
    city_digits = non_digits_remover(city)
    city_numbers = [word for word in city_digits.split() if word.isdigit()]
    assert len(city_numbers) == 1
    return city_numbers[0].zfill(2)
