import requests

from settings import METABASE_USERNAME, METABASE_PASSWORD, METABASE_BASE_URL


def metabase_get_token():
    payload = {"username": METABASE_USERNAME, "password": METABASE_PASSWORD}
    r = requests.post(METABASE_BASE_URL + "/api/session", json=payload)
    if r.status_code != 200:
        raise ValueError("We could not get token. Endpoint returned with status code '{}', with message : '{}'"
                         .format(r.status_code, r.text[:300]))
    return r.json()['id']


def metabase_get(endpoint, token):
    r = requests.get(METABASE_BASE_URL + endpoint, headers={"X-Metabase-Session": token})
    if r.status_code != 200:
        raise ValueError("Get requests on endpoint '{}' returned with non 200 status code '{}' and message '{}'"
                         .format(endpoint, r.status_code, r.text[:300]))
    return r.json()


def metabase_post(endpoint, token, payload=None):
    if payload is None:
        payload = {}
    r = requests.post(METABASE_BASE_URL + endpoint, headers={"X-Metabase-Session": token}, json=payload)
    if r.status_code != 200:
        raise ValueError("Post requests on endpoint '{}' returned with non 200 status code '{}' and message '{}'"
                         .format(endpoint, r.status_code, r.text[:300]))
    return r.json()


def metabase_query_card(card_id, token, ignore_cache=True):
    endpoint = "/api/card/{}/query".format(card_id)
    payload = {"ignore_cache": ignore_cache}
    return metabase_post(endpoint, token, payload)


def metabase_query_all_cards(cards, token, ignore_cache=True):
    for card in sorted(cards, key=lambda x: x['id']):
        result = metabase_query_card(card['id'], token, ignore_cache)
        if result['status'] == 'completed':
            print("Question completed in {time:>4}ms - {id:>3}: {name} "
                  .format(id=card['id'], name=card['name'], time=result['running_time']))
        else:
            print("Question failed - {id:>3}: {name} - {url}/question/{id}"
                  .format(id=card['id'], name=card['name'], url=METABASE_BASE_URL))


def find_dashboards_containing_card(card_id, token):
    dashboards_containing_card = list()
    shallow_dashboard_list = metabase_get("/api/dashboard/", token)
    for shallow_dashboard in shallow_dashboard_list:
        dashboard_id = shallow_dashboard['id']
        dashboard = metabase_get("/api/dashboard/{}".format(dashboard_id), token)
        for ordered_card in dashboard['ordered_cards']:
            if ordered_card['card_id'] == card_id:
                print("Card is present in dashboard {id:>3} : {name}"
                      .format(id=dashboard_id, name=dashboard['name']))
                dashboards_containing_card.append(dashboard_id)

    if not dashboards_containing_card:
        print("Card was not found in any active dashboard")
    return dashboards_containing_card


if __name__ == '__main__':
    main_token = metabase_get_token()
    main_cards = metabase_get("/api/card/", main_token)
    metabase_query_all_cards(main_cards, main_token)
    find_dashboards_containing_card(7, main_token)
