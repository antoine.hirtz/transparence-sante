import os

from dotenv import load_dotenv

from src.constants.connexions import get_default_postgres_host, get_default_metabase_host

load_dotenv()

ROWS_LIMIT = os.getenv('ROWS_LIMIT')
if ROWS_LIMIT is not None:
    ROWS_LIMIT = int(ROWS_LIMIT)

POSTGRES_HOST = os.getenv('POSTGRES_HOST', get_default_postgres_host())
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD', '')
POSTGRES_PORT = int(os.getenv('POSTGRES_PORT', '5432'))

# Memory related
POSTGRES_WORK_MEM = os.getenv('POSTGRES_WORK_MEM', '150MB')
CSV_CHUNK_SIZE = int(os.getenv('CSV_CHUNK_SIZE', 10 ** 5))

# Only for tests. Defined in pytest.ini
DATA_ROOT_DIR = os.getenv('DATA_ROOT_DIR', '')

METABASE_USERNAME = os.getenv('METABASE_USERNAME')
METABASE_PASSWORD = os.getenv('METABASE_PASSWORD')
METABASE_HOST = os.getenv('METABASE_HOST', get_default_metabase_host())
METABASE_BASE_URL = "http://{}:3000".format(METABASE_HOST)
