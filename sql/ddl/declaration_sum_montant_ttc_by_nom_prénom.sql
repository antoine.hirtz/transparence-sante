
SET work_mem = '150MB'; -- allow HashAggregate rather than a slow GroupAggregate
CREATE MATERIALIZED VIEW "declaration_sum_montant_ttc_by_nom_prénom" AS (
    SELECT
        "nom_prénom",
        sum("montant_ttc") AS "sum_montant_ttc"
    FROM "declaration"
    WHERE (
        ("nom_prénom" IS NOT NULL)
        AND ("montant_ttc" IS NOT NULL)
    )
    GROUP BY "nom_prénom"
    ORDER BY (sum("montant_ttc")) DESC
);
RESET work_mem;

CREATE INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_nom_prénom_prefix_index_on_nom_prénom"
ON public."declaration_sum_montant_ttc_by_nom_prénom" (lower("nom_prénom") text_pattern_ops, "nom_prénom");

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_nom_prénom_index_on_nom_prénom"
ON public."declaration_sum_montant_ttc_by_nom_prénom" ("nom_prénom");

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_nom_prénom_index_on_sum_montant_ttc"
ON public."declaration_sum_montant_ttc_by_nom_prénom" ("sum_montant_ttc");
