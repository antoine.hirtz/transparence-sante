
SET work_mem = '150MB'; -- allow HashAggregate rather than a slow GroupAggregate
CREATE MATERIALIZED VIEW "declaration_sum_montant_ttc_by_specialité_annee" AS (
    SELECT
        "specialité", "annee",
        sum("montant_ttc") AS "sum_montant_ttc"
    FROM "declaration"
    WHERE (
        ("specialité" IS NOT NULL) AND ("annee" IS NOT NULL)
        AND ("montant_ttc" IS NOT NULL)
    )
    GROUP BY "specialité", "annee"
    ORDER BY (sum("montant_ttc")) DESC
);
RESET work_mem;

CREATE INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_specialité_annee_prefix_index_on_specialité"
ON public."declaration_sum_montant_ttc_by_specialité_annee" (lower("specialité") text_pattern_ops, "specialité");

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_specialité_annee_index_on_specialité"
ON public."declaration_sum_montant_ttc_by_specialité_annee" ("specialité");

CREATE INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_specialité_annee_prefix_index_on_annee"
ON public."declaration_sum_montant_ttc_by_specialité_annee" (lower("annee") text_pattern_ops, "annee");

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_specialité_annee_index_on_annee"
ON public."declaration_sum_montant_ttc_by_specialité_annee" ("annee");

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_specialité_annee_index_on_sum_montant_ttc"
ON public."declaration_sum_montant_ttc_by_specialité_annee" ("sum_montant_ttc");
