#!/usr/bin/env python3
import sys

from settings import POSTGRES_HOST
from src.constants.connexions import LOCALHOST, DOCKER_POSTGRES_HOST
from src.constants.miscellaneous import ERROR_CODE_INVALID_ARGUMENT
from src.generic.dumper import Dumper
from src.specific import company
from src.specific.declaration import duplicate_amounts_advantage_convention
from src.specific.declaration import remuneration, advantage, convention, common as declaration, \
    view as declaration_view, join_convention_to_other_declarations
from src.specific.download import company_group_downloader
from src.specific.health_directoy import health_professional, health_establishment, health_professional_activities
from src.specific.types import index_list
from src.utils.utils import get_confirmation


def all_downloader():
    return [
        health_professional.downloader,
        company_group_downloader,
        declaration.downloader
    ]


def all_extractors():
    return [health_professional.extractor] + declaration.extractor_list


def download_and_extract():
    for downloader in all_downloader():
        downloader.run()
    for extractor in all_extractors():
        extractor.run()


def reset_download_and_extract():
    for downloader in all_downloader():
        downloader.reset()
    for extractor in all_extractors():
        extractor.reset()


def all_cleaners():
    return [
        health_professional.cleaner,
        health_establishment.cleaner,
        health_professional_activities.cleaner,
        remuneration.cleaner,
        advantage.cleaner,
        convention.cleaner,
        company.cleaner,
        join_convention_to_other_declarations.cleaner
    ]


def clean():
    for cleaner in all_cleaners():
        cleaner.run()


def all_uploaders():
    return [
        health_professional.uploader,
        health_establishment.uploader,
        health_professional_activities.uploader,
        company.uploader,
        remuneration.uploader,
        advantage.uploader,
        join_convention_to_other_declarations.uploader
    ]


def all_views_ddl():
    return ([declaration_view.view()] +
            declaration.materialized_views +
            [duplicate_amounts_advantage_convention.materialized_view])


def all_upload_actions():
    all_indices: index_list = (
            health_establishment.indices +
            health_professional.indices +
            health_professional_activities.indices +
            declaration.indices +
            company.indices
    )

    all_foreign_keys = (
            declaration.foreign_keys +
            health_professional_activities.foreign_keys
    )

    return all_uploaders() + all_views_ddl() + all_indices + all_foreign_keys


def reset_upload():
    for uploader in all_uploaders():
        uploader.reset()


def upload():
    for action in all_upload_actions():
        action.run()


def update_ddl():
    for uploader in all_uploaders():
        uploader.table_ddl.write_ddl()
    for view in all_views_ddl():
        view.write_ddl()


def dump():
    for uploader in all_uploaders():
        dumper = Dumper(directory=uploader.csv_directory, file_name=uploader.csv_name)
        dumper.run()


def process():
    clean()
    reset_upload()
    upload()
    dump()


ACTION_MAPPING = {
    'download': download_and_extract,
    'reset-download': reset_download_and_extract,
    'clean': clean,
    'upload': upload,
    'reset-upload': reset_upload,
    'dump': dump,
    'process': process,
}


def invalid_input():
    print("Main entrypoint to use eurosfordocs project\n")
    print("Usage:")
    for action_key in ACTION_MAPPING.keys():
        print("  ./main.py {}".format(action_key))
    exit(ERROR_CODE_INVALID_ARGUMENT)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        invalid_input()

    action = sys.argv[1]
    if action in ACTION_MAPPING:
        if POSTGRES_HOST not in [LOCALHOST, DOCKER_POSTGRES_HOST]:
            get_confirmation(
                "You are going to execute action '{}' with POSTGRES_HOST='{}'.\nDo you confirm ?"
                    .format(action, POSTGRES_HOST)
            )
        ACTION_MAPPING[action]()
    else:
        invalid_input()
