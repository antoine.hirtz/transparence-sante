# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
# ---

# <h1 id="tocheading">Summary</h1>
# <div id="toc"></div>

# + {"language": "javascript"}
# $.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')
# -

# # Setup

# ## Generic import and setup

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys
ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

# +
import numpy as np
import pandas as pd
pd.options.display.max_rows = 30
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100

import matplotlib.pyplot as plt
from matplotlib_venn import venn2, venn3
from pprint import pprint
# -

# ## Specific import

from src.metabase.api import metabase_get_token, metabase_get, metabase_post, metabase_query_all_cards, \
find_dashboards_containing_card, MB_BASE_URL, metabase_query_card

# # Use metabase API

print(MB_BASE_URL)

# ## Get token and generic elements

token = metabase_get_token()

cards = metabase_get("/api/card/", token)

dashboards = metabase_get("/api/dashboard/", token)

# ## Query postgresql through Metabase

# ### Search

name_surname_query = "a"
pprint(metabase_get("/api/field/146/search/146?value={}&limit=10".format(name_surname_query), token))

# ### Get question result

question_id = 55
metabase_post("/api/card/{}/query/json".format(question_id), token)

# ## Query all cards 

metabase_query_all_cards(cards, token)

# ## Look at specific elements

# ### dashboard

#for card in cards:
#    card_id = card['id']
#    print(card_id, card['name'])
card_id = 384
find_dashboards_containing_card(card_id, token)

# +
def find_card_id_in_cards(card_id):
    for card in cards:
        if card['id'] == card_id:
            return card

card = find_card_id_in_cards(card_id)
result = metabase_query_card(card, token, ignore_cache=True)
# -

card

result

len(dashboards)

pprint(dashboards[0])

# ### card

card_id = 7
for card in cards:
    if card['id'] == card_id:
        pprint(card)

# ### field

pprint(metabase_get("/api/field/146", token))


