#!/usr/bin/env bash
set -euo pipefail

host=${1:-"ts-prod"}

DATE_TAG=`date +%Y-%m-%d_%H-%M-%S`
TAG='_replace_local_from_server'

BACKUP_FOLDER='backup/'${DATE_TAG}${TAG}'/'
METABASE_DB_BACKUP_PATH=${BACKUP_FOLDER}'metabase.db.mv.db'

METABASE_DOCKER_FOLDER='docker/metabase/data/'
METABASE_DB_DOCKER_PATH=${METABASE_DOCKER_FOLDER}'metabase.db.mv.db'


echo Saving metabase data to \'${BACKUP_FOLDER}\'
mkdir -p ${BACKUP_FOLDER}
scp root@${host}:transparence-sante/${METABASE_DB_DOCKER_PATH} ${BACKUP_FOLDER}

if [ ! -f ${METABASE_DB_BACKUP_PATH} ]; then
    echo "File not found! Aborting"
    exit
fi

echo Replacing local metabase data with metabase data backuped from server
echo This will stop Metabase locally
echo This will DESTROY ANY DATA IN LOCAL METABASE

read -p "Continue (I confirm/ No)? " choice
case "$choice" in
  "I confirm") echo "Yes. Going on" ;;
  n|N|no|No ) echo "No. Aborting" ; exit ;;
  * ) echo "invalid. Aborting" ; exit ;;
esac

docker-compose stop metabase
mv ${METABASE_DB_DOCKER_PATH} ${METABASE_DB_BACKUP_PATH}'_local_backup'
mv ${METABASE_DOCKER_FOLDER}'metabase.db.trace.db' ${BACKUP_FOLDER}'metabase.db.trace.db_local_backup'

cp ${METABASE_DB_BACKUP_PATH} ${METABASE_DB_DOCKER_PATH}
docker-compose up --no-deps -d metabase
