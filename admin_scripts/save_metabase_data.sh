#!/usr/bin/env bash
set -euo pipefail

tag='_'${1:-""}
host=${2:-"ts-prod"}

FOLDER=backup/`date +%Y-%m-%d_%H-%M-%S`${tag}

echo Saving metabase data to ${FOLDER}
mkdir -p ${FOLDER}
scp root@${host}:transparence-sante/docker/metabase/data/metabase.db.mv.db ${FOLDER}/