#!/usr/bin/env bash

host=${1:-"ts-prod"}

ssh -t root@${host} "cd transparence-sante && docker-compose stop metabase && docker-compose rm metabase && docker-compose up -d metabase"
