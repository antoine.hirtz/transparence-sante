---
permalink: /presentation/
title: Objectifs du projet
toc: true
---

## Un libre accès aux données…

Entre octobre 2013 et février 2015, l'association Regards Citoyens met en place un [outil de collecte](https://github.com/regardscitoyens/sunshine-data) des déclarations présentes sur la base Transparence-Santé, ainsi que de celles déposées sur les différents sites des ordres avant le lancement du site unique.  

Elle [publie](https://www.regardscitoyens.org/sunshine-ces-245-millions-deuros-que-les-laboratoires-pharmaceutiques-veulent-cacher/) ces données en libre accès en mars 2015, ainsi qu'une précieuse [vision d'ensemble](https://www.regardscitoyens.org/sunshine/).
Les données et analyses sont anonymisées pour respecter un [avis de la CNIL](https://www.regardscitoyens.org/lavis-de-la-cnil-qui-demande-la-confidentialite-des-declarations-publiques-dinterets/#r1). 

En janvier 2016, la loi est modifiée pour autoriser la publication des données de la base Transparence-Santé. 
En mai, elles sont mises à disposition [par Etalab](https://www.etalab.gouv.fr/la-base-transparence-sante-sur-data-gouv-fr) sur le site [Data Gouv](https://www.data.gouv.fr/fr/datasets/transparence-sante-1/).

## … insuffisant pour les exploiter
 
Cependant, les données brutes sont trop volumineuses pour les logiciels d'analyse grand public tels qu'Excel.
De plus, leur médiocre qualité nécessite un important travail de nettoyage.

En pratique, les données ne sont pas exploitables sans compétences avancées en programmation.

De fait, les médias ne profitent pas de cette ouverture pour effectuer des analyses globales sur le lobbying des industries de santé. 
Le journal _Le Monde_ fait exception, avec l'article 
[Comment l’argent des labos irrigue le monde de la santé](https://abonnes.lemonde.fr/economie/article/2017/10/12/les-laboratoires-aux-petits-soins-pour-les-medecins_5199912_3234.html), 
accompagné d'un autre exposant les difficultés techniques rencontrées : 
[Les ratés de la base de données publique Transparence Santé](https://abonnes.lemonde.fr/les-decodeurs/article/2017/10/12/les-rates-de-la-base-de-donnees-publique-transparence-sante_5199937_4355770.html).

## Un premier objectif : simplifier l'exploitation de la base Transparence-Santé

Le projet EurosForDocs est né du décalage entre le potentiel de la base Transparence-Santé et son usage limité. 

L'objectif prioritaire est de simplifier l'accès aux données existantes, pour servir une large gamme d'utilisateurs et d'usages. Par exemple :

- patients - se renseigner sur les liens d'intérêts de leur médecin 
- citoyens - comprendre le lobby de l'industrie sur le système de santé 
- professionnels de santé - se sensibiliser et participer à l'indépendance de la médecine
- administrations publiques - contrôler l'indépendance des experts sanitaires
- Ordre des médecins - croiser avec les données de leur propre système informatique
- journalistes et chercheurs - mener aisément leurs investigations et recherches

Cette première étape doit démontrer la valeur de la transparence, et le caractère fallacieux de prétendues limites techniques. 

Nous défendons plus généralement une transparence totale, telle que défendue par l'Ordre des médecins[^ordre-transparence].

[^ordre-transparence]: [Entretien](https://www.macsf-exerciceprofessionnel.fr/Responsabilite/Humanisme-deontologie/conflits-interets-professionnels-de-sante-laboratoires) en mars 2016 à la MACSF du Docteur Patrick BOUET, président du Conseil National de l’Ordre des Médecins _"Nous pensons que face aux doutes et aux interrogations qui circulent actuellement dans le monde de la santé, **la transparence totale est la seule réponse possible. Sans cette transparence, tout devient suspicieux, à juste titre d’ailleurs. Une application totale de la loi Bertrand est donc la seule solution à nos yeux.** Les patients, les associations, les autres professionnels de santé pourront alors connaître la nature des liens d’intérêt d’un médecin et lui poser toutes les questions qu’ils souhaitent à ce sujet. Tant que ces éléments et ces informations ne sont pas accessibles, le doute est de mise."_

## Concrètement

Ce projet comprend 3 enjeux techniques immédiats :

1. Nettoyer et enrichir la base de données disponible en téléchargement
1. Créer un site web grand public, inspiré du site américain [DollarsForDocs](https://projects.propublica.org/docdollars), avec une présentation pédagogique, des outils de recherche et des analyses thématiques
1. Offrir un outil d'analyse avancé pour les experts
