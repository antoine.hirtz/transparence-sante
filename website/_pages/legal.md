---
permalink: /legal/
title: Informations légales
toc: true
---

## Données personnelles

Les données personnelles collectées sont détaillées sur la [page]({{ base }}/explore) expliquant la connexion à Metabase. Voici des précisions légales.

La collecte des données personnelles est nécessaire à la fourniture de notre service. La base légale de ces traitements repose donc sur notre obligation contractuelle envers vous.

Ces données sont uniquement collectées afin de :
- Permettre la création et la gestion d'un compte utilisateur sur l'outil Metabase ;
- Suivre l'usage de la plateforme et l'améliorer ;
- Le cas échéant, pour permettre à l'équipe d'EurosForDocs de communiquer avec les utilisateurs.

Ces données sont conservées pour une durée de 3 ans à compter de la fin de relation avec l'utilisateur ou du dernier contact, comme recommandé par la CNIL.

Si vous nous soumettez d'autres informations personnelles, par exemple lors d'une prise de contact, elles seront traitées sur la base de votre consentement et uniquement afin de répondre à votre demande.

EurosForDocs ne partage aucune information personnelle avec des tiers, sauf en cas d'obligation légale.

Vous disposez d'un droit d'accès, de rectification, d'effacement de vos données à caractère personnel ainsi que d'un droit de limitation ou d'opposition à leur traitement. Vous pouvez les exercer en contactant EurosForDocs à l'adresse [`contact@eurosfordocs.fr`](mailto:contact@eurosfordocs.fr).

Vous pouvez également saisir la CNIL de toute réclamation.
