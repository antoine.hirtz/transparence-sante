---
permalink: /contributing/
title: Contribuer
---

<i class="far fa-hand-point-right" style="color:Tomato"></i> 
We need YOU 
<i class="far fa-hand-point-left" style="color:Tomato"></i>

<i class="fab fa-connectdevelop fa-fw" style="color:DarkBlue"></i>
EurosForDocs est un **collectif** ouvert à toute bonne volonté.
<i class="fab fa-connectdevelop fa-fw" style="color:DarkBlue"></i>

- <i class="fas fa-edit fa-fw"></i> 
**Rédaction** et relecture : [formulaire](https://goo.gl/forms/fv2ocLYPUHC9aAFd2) pour les suggestions

- <i class="fas fa-laptop-code fa-fw"></i>
**Développement** de la plateforme (cf. [GitLab Issues](https://gitlab.com/eurosfordocs/transparence-sante/issues))

- <i class="fas fa-balance-scale fa-fw"></i>
 Limitation des risques **juridiques**, et analyse des lois

- <i class="fas fa-bullhorn fa-fw"></i>
**Communication**, réseaux sociaux

- <i class="fas fa-map-signs fa-fw"></i>
**Organisation** du site et **graphisme**

- <i class="fas fa-capsules fa-fw"></i>
Connaissances sur le **lobbying** des industriels de santé

- <i class="fa fa-address-book fa-fw"></i>
Contacts et partenariats avec des **acteurs institutionnels** (services de l'État, Conseils des Ordres, CNIL, etc.)

Pour les Parisiens, RDV chaque mercredi avec [Data For Good](https://twitter.com/eurosfordocs/status/1053688319517835264).

<iframe 
    src="https://calendar.google.com/calendar/b/2/embed?showTitle=0&amp;showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=contact%40eurosfordocs.fr&amp;color=%231B887A&amp;ctz=Europe%2FParis" 
    style="border-width:0" 
    width="800" 
    height="150" 
    frameborder="0" 
    scrolling="no">
</iframe>

<i class="fab fa-osi fa-fw"></i> 
EurosForDocs est un projet [libre](https://gitlab.com/eurosfordocs/transparence-sante){:target="_blank"}.
Le code est publié sous licence GNU AGPLv3[^licence], qui garantit à quiconque la liberté de le réutiliser. 

[^licence]: Vous avez le droit de réutiliser, étudier, dupliquer et modifier le code source du projet. La caractéristique [copyleft](https://fr.wikipedia.org/wiki/Copyleft) de la licence impose cependant que toute évolution du code doit être publiée sous les mêmes conditions, sans restrictions de droits.

Voir la liste des [contributeurs au code source](https://gitlab.com/eurosfordocs/transparence-sante/graphs/master){:target="_blank"}. 

<i class="fas fa-envelope fa-fw" style="color:DarkBlue"></i>
Écrivez à [`contact@eurosfordocs.fr`](mailto:contact@eurosfordocs.fr).

<i class="fab fa-angellist fa-fw" style="color:Green"></i> 
Toutes vos remarques ou idées, tous vos encouragements ou contacts sont les bienvenus !

<i class="fas fa-euro-sign fa-fw" style="color:Gold"></i>
Vous pouvez également financer le projet via ce [collectif ouvert](https://opencollective.com/eurosfordocs){:target="_blank"}.
Ce système offre une comptabilité totalement transparente, en accord avec nos principes.

Le projet est bénévole. Les besoins financiers correspondent à l'hébergement du site et à l'organisation de rencontres.  

Votre soutien &mdash; même symbolique &mdash; nous sera très utile, car il démontre l'intérêt citoyen du projet.
